package com.example.cinema.services;

import com.example.cinema.models.Genre;

import java.util.List;

public interface GenreService {

    Genre getById(Integer id);

    List<Genre> getAll();
}
