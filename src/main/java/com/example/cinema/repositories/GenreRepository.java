package com.example.cinema.repositories;

import com.example.cinema.models.Genre;

import java.util.List;

public interface GenreRepository {

    Genre getById(Integer id);

    List<Genre> getAll();
}
