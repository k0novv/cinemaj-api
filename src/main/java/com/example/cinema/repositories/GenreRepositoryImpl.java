package com.example.cinema.repositories;

import com.example.cinema.exceptions.EntityNotFoundException;
import com.example.cinema.models.Genre;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class GenreRepositoryImpl implements GenreRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public GenreRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public Genre getById(Integer id) {
        try (Session session = sessionFactory.openSession()) {

            Optional<Genre> genre = Optional.ofNullable(session.get(Genre.class, id));
            return genre.orElseThrow(() -> new EntityNotFoundException(String.format("Genre with %d, not found", id)));
        }
    }

    @Override
    public List<Genre> getAll() {
        try (Session session = sessionFactory.openSession()) {
            String SQLString = "FROM Genre";
            Query<Genre> genres = session.createQuery(SQLString, Genre.class);
            return genres.list();
        }
    }
}
