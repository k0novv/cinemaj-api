package com.example.cinema.controllers;

import com.example.cinema.exceptions.EntityNotFoundException;
import com.example.cinema.models.Genre;
import com.example.cinema.services.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/genres")
public class GenreControllerImpl implements GenreController {

    private final GenreService service;

    @Autowired
    public GenreControllerImpl(GenreService service) {
        this.service = service;
    }

    @Override
    @GetMapping("/{id}")
    public Genre getById(@PathVariable Integer id) {
        try {
            Genre genre = service.getById(id);
            return genre;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @GetMapping()
    public List<Genre> getAll() {
        return service.getAll();
    }
}
