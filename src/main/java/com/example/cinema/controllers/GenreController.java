package com.example.cinema.controllers;

import com.example.cinema.models.Genre;

import java.util.List;

public interface GenreController {

    Genre getById(Integer id);

    List<Genre> getAll();

}
