package com.example.cinema;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinemaJApplication {

    public static void main(String[] args) {
        SpringApplication.run(CinemaJApplication.class, args);
    }

}
